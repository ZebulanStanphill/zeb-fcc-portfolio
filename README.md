# Zeb’s freeCodeCamp Portfolio

## Description

A portfolio page created for a freeCodeCamp project.

## Licensing info

© 2018-2019 Zebulan Stanphill

This project is released under the GNU General Public License version 3 (or any later version).

Read the license in `LICENSE.txt` or view it on the web:

https://www.gnu.org/licenses/gpl-3.0.html

This project uses the Nunito font:

https://github.com/vernnobile/NunitoFont

© 2014 Vernon Adams (vern@newtypography.co.uk).

Released under the SIL Open Font License, Version 1.1. See `nunito-license.txt`